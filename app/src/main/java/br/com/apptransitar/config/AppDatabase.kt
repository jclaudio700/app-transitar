package br.com.apptransitar.config

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.com.apptransitar.aluno.data.local.dao.AlunoDao
import br.com.apptransitar.aluno.data.local.entity.AlunoEntity

@Database(entities = [AlunoEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract val alunoDao : AlunoDao

    companion object {
        private var INSTANCE : AppDatabase? = null

        fun getInstance(context: Context) : AppDatabase {
            synchronized(this) {
                var instance : AppDatabase? = INSTANCE
                if ( instance == null ) {
                    instance = Room.databaseBuilder(
                        context,
                        AppDatabase::class.java,
                        "db_transitar"
                    ).build()
                }
                return instance
            }
        }
    }
}