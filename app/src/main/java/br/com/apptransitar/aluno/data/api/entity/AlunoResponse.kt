package br.com.apptransitar.aluno.data.api.entity

import java.io.Serializable

data class AlunoResponse(
    val codigo : String,
    val statusAprovacao : String,
    val media : String,
    val message : String
) : Serializable