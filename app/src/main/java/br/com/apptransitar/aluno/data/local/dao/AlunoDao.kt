package br.com.apptransitar.aluno.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import br.com.apptransitar.aluno.data.local.entity.AlunoEntity

@Dao
interface AlunoDao {

    @Insert
    fun insert(alunoEntity: AlunoEntity) : Long

    @Update
    fun update(alunoEntity: AlunoEntity)

    @Query("DELETE FROM aluno WHERE id = :id")
    fun delete(id : Long)

    @Query("SELECT * FROM aluno")
    fun findAll() : List<AlunoEntity>

    @Query("SELECT * FROM aluno WHERE id = :id")
    fun findById(id : Long) : AlunoEntity

}