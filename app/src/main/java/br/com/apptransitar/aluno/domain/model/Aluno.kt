package br.com.apptransitar.aluno.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Aluno(
    var id : Long = 0,
    var codigo : String,
    var nome : String = "",
    var email : String = "",
    var idade : Int = 0,
    var nascimento : String = "",
    var nota1 : String = "",
    var nota2 : String = "",
    var nota3 : String = "",
    var statusAprovacao : String = "",
    var media : String = ""
) : Parcelable