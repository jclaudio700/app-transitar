package br.com.apptransitar.aluno.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "aluno")
data class AlunoEntity(
    @PrimaryKey(autoGenerate = true)
    val id : Long = 0,
    val codigo : String,
    val nome : String = "",
    val email : String = "",
    val idade : Int = 0,
    val nascimento : String = "",
    val nota1 : String = "",
    val nota2 : String = "",
    val nota3 : String = "",
    @ColumnInfo(name = "status_aprovacao")
    val statusAprovacao : String = "",
    val media : String = ""
)