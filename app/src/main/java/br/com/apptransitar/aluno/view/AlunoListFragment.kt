package br.com.apptransitar.aluno.view

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.apptransitar.R
import br.com.apptransitar.aluno.data.api.datasource.AlunoApiDataSourceImpl
import br.com.apptransitar.aluno.data.api.service.AlunoApi
import br.com.apptransitar.aluno.data.local.datasource.AlunoDataSourceImpl
import br.com.apptransitar.aluno.data.repository.AlunoRepositoryImpl
import br.com.apptransitar.aluno.domain.model.Aluno
import br.com.apptransitar.aluno.view.adapter.AlunoListAdapter
import br.com.apptransitar.aluno.view.viewmodel.AlunoListViewModel
import br.com.apptransitar.aluno.view.viewmodel.AlunoListViewModelFactory
import br.com.apptransitar.config.AppDatabase
import br.com.apptransitar.config.RetrofitConfig
import br.com.apptransitar.databinding.FragmentAlunoListBinding
import com.google.android.material.snackbar.Snackbar

class AlunoListFragment : Fragment(R.layout.fragment_aluno_list) {

    lateinit var viewModel : AlunoListViewModel
    lateinit var binding : FragmentAlunoListBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.binding = DataBindingUtil.setContentView(requireActivity(), R.layout.fragment_aluno_list)

        this.binding.lifecycleOwner = this

        setupToolbar()
        setupViewModel()
        observeViewModelEvents()
        configureViewListeners()
    }

    private fun setupToolbar() {
        this.binding.alunoListToolbar.title = getString(R.string.aluno_list_title)
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            AlunoListViewModelFactory(
                AlunoRepositoryImpl(
                    AlunoDataSourceImpl(
                        AppDatabase.getInstance(requireContext()).alunoDao
                    ),
                    AlunoApiDataSourceImpl(
                        RetrofitConfig().retrofit().create(AlunoApi::class.java)
                    )
                )
            )
        ).get(AlunoListViewModel::class.java)
    }

    private fun observeViewModelEvents() {
        viewModel.allAlunoEvent.observe(viewLifecycleOwner) { alunos ->
            if ( alunos.isEmpty() ) {
                binding.layoutRecycler.visibility = View.GONE
                binding.textNenhumAluno.visibility = View.VISIBLE
            } else {
                binding.layoutRecycler.visibility = View.VISIBLE
                binding.textNenhumAluno.visibility = View.GONE
            }

            val alunoListAdapter = AlunoListAdapter(
                    alunos, onEditarClickListener,
                    onMediaClickListener,
                    onExcluirClickListener
            )

            with(binding.recyclerAlunos) {
                setHasFixedSize(true)
                adapter = alunoListAdapter
            }

            viewModel.messageEventData.observe(viewLifecycleOwner) { stringResId ->
                showSnackBar( stringResId )
            }

            viewModel.messageEventErrorData.observe(viewLifecycleOwner) { stringResId ->
                showSnackBar( stringResId )
            }
        }

        viewModel.alunoStateEventData.observe(viewLifecycleOwner) { alunoState ->
            when (alunoState) {
                is AlunoListViewModel.AlunoListState.Loading -> {
                    binding.alunoListProgress.visibility = View.VISIBLE
                }
                is AlunoListViewModel.AlunoListState.Loaded -> {
                    binding.alunoListProgress.visibility = View.GONE
                }
            }
        }
    }

    private val onEditarClickListener: (Aluno) -> Unit = { aluno ->
        val directions = AlunoListFragmentDirections
                .actionAlunoListFragmentToAlunoFormFragment(aluno)
        findNavController().navigate(directions)
    }

    private val onMediaClickListener: (Aluno) -> Unit = { aluno ->
        this.viewModel.calcularMedia( aluno )
    }

    private val onExcluirClickListener: (Aluno) -> Unit = { aluno ->
        openDialog(aluno)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAlunos()
    }

    private fun configureViewListeners() {
        binding.fabAddAluno.setOnClickListener {
            findNavController().navigate(R.id.action_alunoListFragment_to_alunoFormFragment)
        }
    }

    private fun showSnackBar(stringId : Int) {
        Snackbar.make(binding.mainListConstraint, stringId, Snackbar.LENGTH_LONG).show()
    }

    private fun openDialog(aluno : Aluno) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle( getString(R.string.title_excluir_item) )
        builder.setMessage( getString( R.string.message_excluir_aluno ) )
        builder.setPositiveButton( getString( R.string.sim ) ){dialog, which ->
            viewModel.delete(aluno)
        }
        builder.setNegativeButton(getString( R.string.nao ) ){dialog, which ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}