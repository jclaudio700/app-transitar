package br.com.apptransitar.aluno.data.api.datasource

import br.com.apptransitar.aluno.data.api.entity.AlunoRequest
import br.com.apptransitar.aluno.data.api.entity.AlunoResponse
import retrofit2.Response

interface AlunoApiDataSource {

    suspend fun calcularMedia(request : AlunoRequest) : Response<AlunoResponse>

}