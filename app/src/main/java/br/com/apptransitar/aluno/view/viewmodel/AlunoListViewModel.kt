package br.com.apptransitar.aluno.view.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.apptransitar.R
import br.com.apptransitar.aluno.domain.model.Aluno
import br.com.apptransitar.aluno.domain.repository.AlunoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AlunoListViewModel(
    private val repository : AlunoRepository
) : ViewModel() {

    private val _allAlunoEvent = MutableLiveData<List<Aluno>>()
    val allAlunoEvent: LiveData<List<Aluno>>
        get() = _allAlunoEvent

    private val _alunoStateEventData = MutableLiveData<AlunoListState>()
    val alunoStateEventData: LiveData<AlunoListState>
        get() = _alunoStateEventData

    private val _messageEventData = MutableLiveData<Int>()
    val messageEventData: LiveData<Int>
        get() = _messageEventData

    private val _messageEventErrorData = MutableLiveData<Int>()
    val messageEventErrorData: LiveData<Int>
        get() = _messageEventErrorData

    fun getAlunos() = viewModelScope.launch(Dispatchers.IO) {
        _allAlunoEvent.postValue( repository.findAll() )
    }

    fun calcularMedia(aluno : Aluno) {
        _alunoStateEventData.postValue( AlunoListState.Loading )
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.calcularMedia( aluno )
                if ( response.isSuccessful ) {
                    val result = response.body()!!
                    aluno.codigo = result.codigo
                    aluno.statusAprovacao = result.statusAprovacao
                    aluno.media = result.media
                    repository.update( aluno )
                    _allAlunoEvent.postValue( repository.findAll() )
                    _messageEventData.postValue( R.string.message_sucesso_media )
                } else {
                    _messageEventErrorData.postValue(R.string.message_error_web)
                }
                _alunoStateEventData.postValue( AlunoListState.Loaded )
            } catch (ex : Exception) {
                _alunoStateEventData.postValue( AlunoListState.Loaded )
                _messageEventErrorData.postValue(R.string.message_error_web)
                Log.e(TAG, "Errm Web: " + ex.message)
            }
        }
    }

    fun delete(aluno : Aluno) = viewModelScope.launch {
        viewModelScope.launch(Dispatchers.IO) {
            repository.delete(aluno.id)
            _allAlunoEvent.postValue( repository.findAll() )
            _messageEventData.postValue( R.string.excluido_sucesso )
        }
    }

    sealed class AlunoListState {
        object Loading : AlunoListState()
        object Loaded : AlunoListState()
    }

    companion object {
        private val TAG = AlunoListViewModel::class.java.simpleName
    }

}