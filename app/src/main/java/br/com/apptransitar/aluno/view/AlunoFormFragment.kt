package br.com.apptransitar.aluno.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import br.com.apptransitar.R
import br.com.apptransitar.aluno.data.api.datasource.AlunoApiDataSourceImpl
import br.com.apptransitar.aluno.data.api.service.AlunoApi
import br.com.apptransitar.aluno.data.local.datasource.AlunoDataSourceImpl
import br.com.apptransitar.aluno.data.repository.AlunoRepositoryImpl
import br.com.apptransitar.aluno.domain.model.Aluno
import br.com.apptransitar.aluno.view.viewmodel.AlunoFormViewModel
import br.com.apptransitar.aluno.view.viewmodel.AlunoFormViewModelFactory
import br.com.apptransitar.config.AppDatabase
import br.com.apptransitar.config.RetrofitConfig
import br.com.apptransitar.databinding.FragmentAlunoFormBinding
import com.google.android.material.snackbar.Snackbar

class AlunoFormFragment : Fragment(R.layout.fragment_aluno_form) {

    lateinit var viewModel : AlunoFormViewModel
    lateinit var binding : FragmentAlunoFormBinding

    private val args: AlunoFormFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.binding = DataBindingUtil.setContentView(requireActivity(), R.layout.fragment_aluno_form)

        this.binding.lifecycleOwner = this

        setupToolbar()

        args.aluno?.let { aluno ->
            binding.editTextNome.setText(aluno.nome)
            binding.editTextEmail.setText(aluno.email)
            binding.editTextIdade.setText(aluno.idade.toString())
            binding.editTextNascimento.setText(aluno.nascimento)
            binding.editTextNota1.setText(aluno.nota1)
            binding.editTextNota2.setText(aluno.nota2)
            binding.editTextNota3.setText(aluno.nota3)
            this.binding.alunoFormToolbar.title = getString(R.string.title_editar_aluno)
        }

        setupViewModel()
        observeEvents()
        setupListeners()
    }

    private fun setupToolbar() {
        this.binding.alunoFormToolbar.title = getString(R.string.title_novo_aluno)
        this.binding.alunoFormToolbar.inflateMenu(R.menu.aluno_form_menu)
        this.binding.alunoFormToolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.salvar_aluno -> {
                    onSaveListener()
                }
            }
            false
        }
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
                this,
                AlunoFormViewModelFactory(
                        AlunoRepositoryImpl(
                                AlunoDataSourceImpl(
                                        AppDatabase.getInstance(requireContext()).alunoDao
                                ),
                                AlunoApiDataSourceImpl(
                                        RetrofitConfig().retrofit().create(AlunoApi::class.java)
                                )
                        )
                )
        ).get(AlunoFormViewModel::class.java)
    }

    private fun observeEvents() {
        viewModel.alunoStateEventData.observe(viewLifecycleOwner) { alunoState ->
            when (alunoState) {
                is AlunoFormViewModel.AlunoState.Inserted -> {
                    findNavController().popBackStack()
                }
                is AlunoFormViewModel.AlunoState.Updated -> {
                    findNavController().popBackStack()
                }
                is AlunoFormViewModel.AlunoState.Deleted -> {
                    findNavController().popBackStack()
                }
            }
        }

        viewModel.messageEventData.observe(viewLifecycleOwner) { stringResId ->
            Snackbar.make(binding.mainConstraint, stringResId, Snackbar.LENGTH_LONG).show()
        }
    }

    private fun onSaveListener() {
        if ( isValidate() ) {
            val nome = this.binding.editTextNome.text.toString()
            val email = this.binding.editTextEmail.text.toString()
            val idade = this.binding.editTextIdade.text.toString()
            val nascimento = this.binding.editTextNascimento.text.toString()
            val nota1 = this.binding.editTextNota1.text.toString()
            val nota2 = this.binding.editTextNota2.text.toString()
            val nota3 = this.binding.editTextNota3.text.toString()
            val aluno = Aluno(
                    id = args.aluno?.id ?: 0,
                    codigo = "",
                    nome = nome,
                    email = email,
                    idade = if ( idade.isEmpty() ) 0 else idade.toInt(),
                    nascimento = nascimento,
                    nota1 = if ( nota1.isEmpty() ) "0" else nota1,
                    nota2 = if ( nota2.isEmpty() ) "0" else nota2,
                    nota3 = if ( nota3.isEmpty() ) "0" else nota3
            )
            viewModel.addOrUpdate(aluno)
        }
    }

    private fun isValidate(): Boolean =
            validateNome() && validateEmail() && validateIdade() && validateNascimento()

    private fun setupListeners() {
        binding.editTextNome.addTextChangedListener(TextFieldValidation(binding.editTextNome))
        binding.editTextEmail.addTextChangedListener(TextFieldValidation(binding.editTextEmail))
        binding.editTextIdade.addTextChangedListener(TextFieldValidation(binding.editTextIdade))
        binding.editTextNascimento.addTextChangedListener(TextFieldValidation(binding.editTextNascimento))
    }

    inner class TextFieldValidation(private val view: View) : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            when (view.id) {
                R.id.editTextNome -> {
                    validateNome()
                }
                R.id.editTextEmail -> {
                    validateEmail()
                }
                R.id.editTextIdade -> {
                    validateIdade()
                }
                R.id.editTextNascimento -> {
                    validateNascimento()
                }
            }
        }
    }

    private fun validateNome() : Boolean {
        if (this.binding.editTextNome.text.toString().trim().isEmpty()) {
            this.binding.textInputNome.error = getString(R.string.campo_requerido)
            this.binding.editTextNome.requestFocus()
            return false
        } else {
            this.binding.textInputNome.isErrorEnabled = false
        }
        return true
    }

    private fun validateEmail() : Boolean {
        if (this.binding.editTextEmail.text.toString().trim().isEmpty()) {
            this.binding.textInputemail.error = getString(R.string.campo_requerido)
            this.binding.editTextEmail.requestFocus()
            return false
        } else {
            this.binding.textInputemail.isErrorEnabled = false
        }
        return true
    }

    private fun validateIdade() : Boolean {
        if (this.binding.editTextIdade.text.toString().trim().isEmpty()) {
            this.binding.textInputIdade.error = getString(R.string.campo_requerido)
            this.binding.editTextIdade.requestFocus()
            return false
        } else {
            this.binding.textInputIdade.isErrorEnabled = false
        }
        return true
    }

    private fun validateNascimento() : Boolean {
        if (this.binding.editTextNascimento.text.toString().trim().isEmpty()) {
            this.binding.textInputNascimento.error = getString(R.string.campo_requerido)
            this.binding.editTextNascimento.requestFocus()
            return false
        } else {
            this.binding.textInputNascimento.isErrorEnabled = false
        }
        return true
    }

}