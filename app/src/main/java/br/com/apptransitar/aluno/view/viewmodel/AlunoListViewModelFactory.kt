package br.com.apptransitar.aluno.view.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import br.com.apptransitar.aluno.domain.repository.AlunoRepository

class AlunoListViewModelFactory(
    private val repository : AlunoRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(AlunoListViewModel::class.java)){
            return AlunoListViewModel(repository) as T
        }

        throw IllegalArgumentException("Unknown class name")
    }
}