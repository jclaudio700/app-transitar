package br.com.apptransitar.aluno.view.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.apptransitar.R
import br.com.apptransitar.aluno.domain.model.Aluno
import br.com.apptransitar.aluno.domain.repository.AlunoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AlunoFormViewModel(
    private val repository : AlunoRepository
) : ViewModel() {

    private val _alunoStateEventData = MutableLiveData<AlunoState>()
    val alunoStateEventData: LiveData<AlunoState>
        get() = _alunoStateEventData

    private val _messageEventData = MutableLiveData<Int>()
    val messageEventData: LiveData<Int>
        get() = _messageEventData

    fun addOrUpdate(aluno : Aluno) =
            viewModelScope.launch(Dispatchers.IO) {
        if (aluno.id > 0) {
            update(aluno)
        } else {
            insert(aluno)
        }
    }

    private fun update(aluno : Aluno) =
            viewModelScope.launch(Dispatchers.IO) {
        try {
            repository.update(aluno)

            _alunoStateEventData.postValue(AlunoState.Updated)
            _messageEventData.postValue(R.string.aluno_updated_successfully)
        } catch (ex: Exception) {
            _messageEventData.postValue(R.string.aluno_error_to_update)
            Log.e(TAG, ex.toString())
        }
    }

    private fun insert(aluno : Aluno) =
            viewModelScope.launch(Dispatchers.IO) {
        try {
            val id = repository.insert(aluno)
            if (id > 0) {
                _alunoStateEventData.postValue(AlunoState.Inserted)
                _messageEventData.postValue(R.string.aluno_inserted_successfully)
            }
        } catch (ex: Exception) {
            _messageEventData.postValue(R.string.aluno_error_to_insert)
            Log.e(TAG, ex.toString())
        }
    }

    fun remove(id: Long) = viewModelScope.launch(Dispatchers.IO) {
        try {
            if (id > 0) {
                repository.delete(id)
                _alunoStateEventData.postValue(AlunoState.Deleted)
                _messageEventData.postValue(R.string.aluno_deleted_successfully)
            }
        } catch (ex: Exception) {
            _messageEventData.postValue(R.string.aluno_error_to_delete)
            Log.e(TAG, ex.toString())
        }

    }

    sealed class AlunoState {
        object Inserted : AlunoState()
        object Updated : AlunoState()
        object Deleted : AlunoState()
    }

    companion object {
        private val TAG = AlunoFormViewModel::class.java.simpleName
    }

}