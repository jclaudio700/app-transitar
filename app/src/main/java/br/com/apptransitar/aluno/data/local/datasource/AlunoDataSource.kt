package br.com.apptransitar.aluno.data.local.datasource

import br.com.apptransitar.aluno.data.local.entity.AlunoEntity

interface  AlunoDataSource {

    suspend  fun insert(alunoentity : AlunoEntity) : Long

    suspend  fun update(alunoentity: AlunoEntity)

    suspend  fun delete(id : Long)

    suspend  fun findAll() : List<AlunoEntity>

    suspend  fun findById(id : Long) : AlunoEntity

}