package br.com.apptransitar.aluno.data.mapper

import br.com.apptransitar.aluno.data.api.entity.AlunoRequest
import br.com.apptransitar.aluno.data.local.entity.AlunoEntity
import br.com.apptransitar.aluno.domain.model.Aluno

class AlunoMapper {

    fun domainToEntity(aluno : Aluno) : AlunoEntity {
        return AlunoEntity(
            id = aluno.id,
            codigo = aluno.codigo,
            nome = aluno.nome,
            email = aluno.email,
            idade = aluno.idade,
            nascimento = aluno.nascimento,
            nota1 = aluno.nota1,
            nota2 = aluno.nota2,
            nota3 = aluno.nota3,
            statusAprovacao =  aluno.statusAprovacao,
            media = aluno.media
        )
    }

    fun entityToDomain(alunoEntity : AlunoEntity) : Aluno {
        return Aluno(
            alunoEntity.id,
            alunoEntity.codigo,
            alunoEntity.nome,
            alunoEntity.email,
            alunoEntity.idade,
            alunoEntity.nascimento,
            alunoEntity.nota1,
            alunoEntity.nota2,
            alunoEntity.nota3,
            alunoEntity.statusAprovacao,
            alunoEntity.media
        )
    }

    fun listEntityToListDomain(entities : List<AlunoEntity>) : List<Aluno> {
        val alunos : ArrayList<Aluno> = ArrayList()
        entities.forEach { alunos.add( entityToDomain(it) ) }
        return alunos
    }

    fun domainToRequest(aluno : Aluno) : AlunoRequest {
        return AlunoRequest(
            codigo = aluno.codigo,
            nome = aluno.nome,
            email = aluno.email,
            idade = aluno.idade,
            nascimento = aluno.nascimento,
            nota1 = aluno.nota1,
            nota2 = aluno.nota2,
            nota3 = aluno.nota3
        )
    }

}