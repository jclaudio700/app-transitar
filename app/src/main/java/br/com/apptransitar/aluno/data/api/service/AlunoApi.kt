package br.com.apptransitar.aluno.data.api.service

import br.com.apptransitar.aluno.data.api.entity.AlunoRequest
import br.com.apptransitar.aluno.data.api.entity.AlunoResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AlunoApi {

    @POST("media")
    suspend fun calcularMedia(@Body request : AlunoRequest) : Response<AlunoResponse>

}