package br.com.apptransitar.aluno.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.apptransitar.R
import br.com.apptransitar.aluno.domain.model.Aluno
import kotlinx.android.synthetic.main.item_aluno.view.*

class AlunoListAdapter(
    private val alunos: List<Aluno>,
    private val onEditarClickListener: (aluno: Aluno) -> Unit,
    private val onMediaClickListener: (aluno: Aluno) -> Unit,
    private val onExcluirClickListener: (aluno: Aluno) -> Unit
) : RecyclerView.Adapter<AlunoListAdapter.AlunoListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlunoListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_aluno, parent, false)
        return AlunoListViewHolder(view, onEditarClickListener, onMediaClickListener,
                onExcluirClickListener)
    }

    override fun onBindViewHolder(holder: AlunoListViewHolder, position: Int) {
        holder.bindView(alunos[position])
    }

    override fun getItemCount(): Int = alunos.size

    class AlunoListViewHolder(
        itemView: View,
        private val onEditarClickListener: (aluno: Aluno) -> Unit,
        private val onMediaClickListener: (aluno: Aluno) -> Unit,
        private val onExcluirClickListener: (aluno: Aluno) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {
        private val textViewNome: TextView = itemView.text_aluno_nome
        private val textViewEmail: TextView = itemView.text_aluno_email
        private val textViewStatus: TextView = itemView.text_aluno_status
        private val textViewMedia: TextView = itemView.text_aluno_media
        private val layoutMedia: RelativeLayout = itemView.layout_media

        fun bindView(aluno: Aluno) {
            textViewNome.text = aluno.nome
            textViewEmail.text = itemView.context.getString(
                R.string.resultado_email, aluno.email )
            showOrHideREsultadoMedia( aluno )

            itemView.button_editar.setOnClickListener {
                onEditarClickListener.invoke(aluno)
            }

            itemView.button_excluir.setOnClickListener {
                onExcluirClickListener.invoke(aluno)
            }

            itemView.button_calcular_media.setOnClickListener {
                onMediaClickListener.invoke(aluno)
            }
        }

        private fun showOrHideREsultadoMedia(aluno : Aluno) {
            if ( aluno.statusAprovacao.isEmpty() ) {
                layoutMedia.visibility = View.GONE
            } else {
                layoutMedia.visibility = View.VISIBLE
                textViewStatus.text = itemView.context.getString(
                        R.string.resultado_status, aluno.statusAprovacao )
                textViewMedia.text = itemView.context.getString(
                        R.string.resultado_media, aluno.media )
            }
        }
    }

}