package br.com.apptransitar.aluno.data.api.datasource

import br.com.apptransitar.aluno.data.api.entity.AlunoRequest
import br.com.apptransitar.aluno.data.api.entity.AlunoResponse
import br.com.apptransitar.aluno.data.api.service.AlunoApi
import retrofit2.Response

class AlunoApiDataSourceImpl(
    val alunoApi : AlunoApi,
) : AlunoApiDataSource {

    override suspend fun calcularMedia(request: AlunoRequest): Response<AlunoResponse> =
        alunoApi.calcularMedia(request)
}
