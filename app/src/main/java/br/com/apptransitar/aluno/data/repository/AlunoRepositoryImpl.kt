package br.com.apptransitar.aluno.data.repository

import br.com.apptransitar.aluno.data.api.datasource.AlunoApiDataSource
import br.com.apptransitar.aluno.data.api.entity.AlunoResponse
import br.com.apptransitar.aluno.data.local.datasource.AlunoDataSource
import br.com.apptransitar.aluno.data.mapper.AlunoMapper
import br.com.apptransitar.aluno.domain.model.Aluno
import br.com.apptransitar.aluno.domain.repository.AlunoRepository
import retrofit2.Response

class AlunoRepositoryImpl(
    private val dataSourceDb : AlunoDataSource,
    private val dataSourceApi : AlunoApiDataSource
) : AlunoRepository {

    private val mapper = AlunoMapper()

    override suspend fun insert(aluno: Aluno): Long =
        this.dataSourceDb.insert( mapper.domainToEntity( aluno ) )

    override suspend fun update(aluno: Aluno) =
        this.dataSourceDb.update( mapper.domainToEntity( aluno ) )

    override suspend fun delete(id: Long) = this.dataSourceDb.delete( id )

    override suspend fun findAll(): List<Aluno> =
        this.mapper.listEntityToListDomain( this.dataSourceDb.findAll() )

    override suspend fun findById(id: Long): Aluno {
        val res = this.dataSourceDb.findById(id)
        return mapper.entityToDomain( res )
    }

    override suspend fun calcularMedia(aluno : Aluno) : Response<AlunoResponse> =
        this.dataSourceApi.calcularMedia( this.mapper.domainToRequest( aluno ) )
}