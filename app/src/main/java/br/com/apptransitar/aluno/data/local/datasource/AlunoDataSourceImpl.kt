package br.com.apptransitar.aluno.data.local.datasource

import br.com.apptransitar.aluno.data.local.dao.AlunoDao
import br.com.apptransitar.aluno.data.local.entity.AlunoEntity

class AlunoDataSourceImpl(
    private val alunoDao: AlunoDao
) : AlunoDataSource {

    override suspend fun insert(alunoentity: AlunoEntity): Long = this.alunoDao.insert(alunoentity)

    override suspend fun update(alunoentity: AlunoEntity) {
        this.alunoDao.update(alunoentity)
    }

    override suspend fun delete(id: Long) {
        this.alunoDao.delete(id)
    }

    override suspend fun findAll(): List<AlunoEntity> = this.alunoDao.findAll()

    override suspend fun findById(id: Long): AlunoEntity = this.alunoDao.findById(id)
}