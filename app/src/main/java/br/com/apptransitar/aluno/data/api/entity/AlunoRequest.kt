package br.com.apptransitar.aluno.data.api.entity

import com.google.gson.annotations.SerializedName

data class AlunoRequest(
    @SerializedName("codigo")
    val codigo : String,
    @SerializedName("nome")
    val nome : String,
    @SerializedName("email")
    val email : String,
    @SerializedName("idade")
    val idade : Int,
    @SerializedName("nascimento")
    val nascimento : String,
    @SerializedName("nota1")
    val nota1 : String,
    @SerializedName("nota2")
    val nota2 : String,
    @SerializedName("nota3")
    val nota3 : String
)