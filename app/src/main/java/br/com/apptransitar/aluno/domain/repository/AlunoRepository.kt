package br.com.apptransitar.aluno.domain.repository

import br.com.apptransitar.aluno.data.api.entity.AlunoResponse
import br.com.apptransitar.aluno.domain.model.Aluno
import retrofit2.Response

interface AlunoRepository {

    suspend fun insert(aluno : Aluno) : Long

    suspend fun update(aluno: Aluno)

    suspend fun delete(id : Long)

    suspend fun findAll() : List<Aluno>

    suspend fun findById(id : Long) : Aluno

    suspend fun calcularMedia(aluno : Aluno) : Response<AlunoResponse>

}